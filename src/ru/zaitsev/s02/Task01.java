package ru.zaitsev.s02;

public class Task01 {
    public static int sumup_2numbers(int figure) {
        int f_fig = figure / 10;
        int s_fig = figure % 10;

        return f_fig + s_fig;
    }


    public static void main(String[] args) {
        System.out.println("Задача 1. Сумма чисел двухщначного числа " + sumup_2numbers(57));
    }
}
